<?php
require_once '../modelos/Clientes.php';

$clientes = new Clientes();

$idclientes = isset($_POST['idclientes'])?limpiarcadena($_POST['idclientes']):"";

$idmovimiento = isset($_POST['movimiento'])?limpiarcadena($_POST['movimiento']):"";
$persona = isset($_POST['persona'])?limpiarcadena($_POST['persona']):"";
$celular = isset($_POST['numero_celular'])?limpiarcadena($_POST['numero_celular']):"";

$idplazo = isset($_POST['plazo'])?limpiarcadena($_POST['plazo']):"";
$idformadepago = isset($_POST['forma_pago'])?limpiarcadena($_POST['forma_pago']):"";

$nombre = isset($_POST['nombre'])?limpiarcadena($_POST['nombre']):"";
$apellidop = isset($_POST['apellido_paterno'])?limpiarcadena($_POST['apellido_paterno']):"";
$apellidom = isset($_POST['apellido_materno'])?limpiarcadena($_POST['apellido_materno']):"";
$fechanac = isset($_POST['fechanac'])?limpiarcadena($_POST['fechanac']):"";
$rfc = isset($_POST['rfc'])?limpiarcadena($_POST['rfc']):"";
$apoderadolegal = isset($_POST['apoderado_legal'])?limpiarcadena($_POST['apoderado_legal']):"";
$rfcempresa = isset($_POST['rfc_empresa'])?limpiarcadena($_POST['rfc_empresa']):"";

$calle = isset($_POST['calle'])?limpiarcadena($_POST['calle']):"";
$numexterior = isset($_POST['numero_exterior'])?limpiarcadena($_POST['numero_exterior']):"";
$numinterior = isset($_POST['numero_interior'])?limpiarcadena($_POST['numero_interior']):"";
$colonia = isset($_POST['colonia'])?limpiarcadena($_POST['colonia']):"";
$ciudad = isset($_POST['ciudad'])?limpiarcadena($_POST['ciudad']):"";
$municipio = isset($_POST['municipio'])?limpiarcadena($_POST['municipio']):"";
$estado = isset($_POST['estado'])?limpiarcadena($_POST['estado']):"";
$codigopostal = isset($_POST['codigo_postal'])?limpiarcadena($_POST['codigo_postal']):"";
$numcontacto = isset($_POST['numcontacto'])?limpiarcadena($_POST['numcontacto']):"";

$burodecredito = isset($_POST['buro_de_credito'])?limpiarcadena($_POST['buro_de_credito']):"";
$tarjetadecredito = isset($_POST['tarjeta_credito'])?limpiarcadena($_POST['tarjeta_credito']):"";
$creditohipotecario = isset($_POST['credito_hipotecario'])?limpiarcadena($_POST['credito_hipotecario']):"";
$creditoautomotriz = isset($_POST['credito_automotriz'])?limpiarcadena($_POST['credito_automotriz']):"";
$autorizo1 = isset($_POST['autorizo1'])?limpiarcadena($_POST['autorizo1']):"";
$autorizo2 = isset($_POST['autorizo2'])?limpiarcadena($_POST['autorizo2']):"";

$lugarnac = isset($_POST['lugar_nac'])?limpiarcadena($_POST['lugar_nac']):"";
$tipoidentificacion = isset($_POST['tipo_identificacion'])?limpiarcadena($_POST['tipo_identificacion']):"";
$folioidentificacion = isset($_POST['folio_identificacion'])?limpiarcadena($_POST['folio_identificacion']):"";
$sexo = isset($_POST['sexo'])?limpiarcadena($_POST['sexo']):"";
$curp = isset($_POST['curp'])?limpiarcadena($_POST['curp']):"";
$correo = isset($_POST['correo'])?limpiarcadena($_POST['correo']):"";
$confirmarcorreo = isset($_POST['confirmar_correo'])?limpiarcadena($_POST['confirmar_correo']):"";
$banco = isset($_POST['banco'])?limpiarcadena($_POST['banco']):"";

$telefono = isset($_POST['telefono'])?limpiarcadena($_POST['telefono']):"";
$referencias = isset($_POST['referencias_dom'])?limpiarcadena($_POST['referencias_dom']):"";
$entrecalle = isset($_POST['entre_calle'])?limpiarcadena($_POST['entre_calle']):"";
$ycalle = isset($_POST['y_calle'])?limpiarcadena($_POST['y_calle']):"";

$ocupacion = isset($_POST['ocupacion'])?limpiarcadena($_POST['ocupacion']):"";
$empresa = isset($_POST['empresa'])?limpiarcadena($_POST['empresa']):"";
$telefonoempresa = isset($_POST['telefono_empresa'])?limpiarcadena($_POST['telefono_empresa']):"";
$antiguedad = isset($_POST['antiguedad_empresa'])?limpiarcadena($_POST['antiguedad_empresa']):"";

switch ($_GET["op"]) {
    case 'agregaryeditar':
        if (empty($idclientes)) {
            $result = $clientes->agregar($idmovimiento,$persona,$celular,$idplazo,$idformadepago,$nombre,$apellidop,$apellidom,$rfc,$fechanac,$apoderadolegal,$rfcempresa,$calle,$numexterior,$numinterior,$colonia,$ciudad,$municipio,$estado,$codigopostal,$numcontacto,$burodecredito,$tarjetadecredito,$creditohipotecario,$creditoautomotriz,$autorizo1,$autorizo2,$lugarnac,$tipoidentificacion,$folioidentificacion,$sexo,$curp,$correo,$confirmarcorreo,$banco,$telefono,$referencias,$entrecalle,$ycalle,$ocupacion,$empresa,$telefonoempresa,$antiguedad);
            echo $result;
        } else {
            $result = $clientes->editar($idclientes,$idmovimiento,$persona,$celular,$idplazo,$idformadepago,$nombre,$apellidop,$apellidom,$rfc,$fechanac,$apoderadolegal,$rfcempresa,$calle,$numexterior,$numinterior,$colonia,$ciudad,$municipio,$estado,$codigopostal,$numcontacto,$burodecredito,$tarjetadecredito,$creditohipotecario,$creditoautomotriz,$autorizo1,$autorizo2,$lugarnac,$tipoidentificacion,$folioidentificacion,$sexo,$curp,$correo,$confirmarcorreo,$banco,$telefono,$referencias,$entrecalle,$ycalle,$ocupacion,$empresa,$telefonoempresa,$antiguedad);
            echo $result;
        }

        break;

    case 'mostrar':
        $result = $clientes->mostrar($idclientes);
        echo json_encode($result);
        break;

    case 'listar':
        $result=$clientes->listar();
        $data=array();

        while ($reg=$result->fetch_object()) {
            $data[]=array(
                "0" => $reg->idclientes,
                "1" => $reg->persona,
                "2" => $reg->celular,
                "3" => $reg->nombre,
                "4" => $reg->apellidop .' '.$reg->apellidom,
                "5" => $reg->rfc,
                "6" => '<div class="dropdown custom-dropdown">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-horizontal"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                </a>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink1" style="will-change: transform;">
                    <a class="dropdown-item" href="seclienteview.php?cliente='.$reg->idclientes.'">Ver</a>
                    <a class="dropdown-item" href="javascript:void(0);">Editar</a>
                </div>
            </div>'
            );
        }
        $results = array(
            "sEcho"=>1,
            "iTotalRecords"=>count($data),
            "iTotalDisplayRecords"=>count($data),
            "aaData"=>$data);
        echo json_encode($results);

        break;
    default:
        echo "OPCION NO ENCONTRADA";
        break;
}