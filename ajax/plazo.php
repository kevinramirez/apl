<?php
require_once "../modelos/Plazo.php";

$plazo = new Plazo();

$idplazo = isset($_POST['idplazo'])?limpiarCadena($_POST["idplazo"]):"";
$descripcion = isset($_POST["descripcion"])?limpiarCadena($_POST["descripcion"]):"";

switch ($_GET["op"]) {
    case 'agregaryeditar':
        if (empty($idplazo)) {
            $result=$plazo->agregar($descripcion);
            echo $result;
        } else {
            $result=$plazo->editar($idplazo,$descripcion);
            echo $result;
        }
        
        break;

    case 'mostrar':
        $result=$plazo->mostrar($idplazo);
        echo json_encode($result);
        break;

    case 'listar':
        $result=$plazo->listar();
        $data=array();

        while ($reg=$result->fetch_object()) {
            $data[] = array(
                "0" => $reg->idplazo,
                "1" => $reg->descripcion
            );
        }
        $results = array(
            "sEcho"=>1,
            "iTotalRecords"=>count($data),
            "iTotalDisplayRecords"=>count($data),
            "aaData"=>$data);
        echo json_encode($results);

        break;
    
    default:
        echo "OPCION NO ENCONTRADA";
        break;
}