<?php
require_once "../modelos/Usuarios.php";

$usuarios = new Usuarios();

$idusuarios=isset($_POST["idusuarios"])? limpiarCadena($_POST["idusuarios"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";
$apellidos=isset($_POST["apellidos"])? limpiarCadena($_POST["apellidos"]):"";
$image=isset($_POST["image"])? limpiarCadena($_POST["image"]):"";
$correo=isset($_POST["correo"])? limpiarCadena($_POST["correo"]):"";
$password=isset($_POST["password"])? limpiarCadena($_POST["password"]):"";
$status=isset($_POST["status"])? limpiarCadena($_POST["status"]):"";
$role=isset($_POST["role"])? limpiarCadena($_POST["role"]):"";

switch ($_GET["op"]) {
    case 'agregaryeditar':
        $caracteres = "abcdefghijklmnopqrstuvwxyz1234567890";
        $nueva_clave = "";
        for ($i = 5; $i < 35; $i++) {
            $nueva_clave .= $caracteres[rand(5,35)];
        }

        $aleatorio = $nueva_clave;
        $valor = "07";
        $salt = "$2y$".$valor."$".$aleatorio."$";

        $passwordConSalt = crypt($password, $salt);

        if (empty($idusuarios)) {
            $result=$usuarios->agregar($nombre,$apellidos,$correo,$passwordConSalt,$role);
            echo $result;
        } else {
            $result=$usuarios->editar($idusuarios,$nombre,$apellidos,$correo,$role);
            echo $result;
        }

        break;

    case 'desactivar':
        $result=$usuarios->desactivar($idusuarios);
            echo $result;
        break;

    case 'activar':
        $result=$usuarios->activar($idusuarios);
            echo $result;
        break;

    case 'mostrar':
        $result=$usuarios->mostrar($idusuarios);
            echo json_encode($result);
        break;

    case 'listar':
        $result=$usuarios->listar();
        $data= Array();
         
        while ($reg=$result->fetch_object()) {
            $data[]=array(
                "0" => $reg->idusuarios,
                "1" => $reg->nombre,
                "2" => $reg->apellidos,
                "3" => $reg->correo,
                "4" => $reg->status,
                "5" => $reg->role
            );
        }
        $results = array(
            "sEcho"=>1,
            "iTotalRecords"=>count($data),
            "iTotalDisplayRecords"=>count($data),
            "aaData"=>$data);
        echo json_encode($results);
        break;

    case 'verificar':
        $email = $_POST['email'];
        $password = $_POST['password'];

        $result = $usuarios->verificar($email)->fetch_object();

        if (isset($result)) {
            if ( ($email == $result->correo) && ( password_verify($password, $result->password) ) && ( $result->status == '1') ) {

                $_SESSION['idusuario']=$result->idusuarios;
                $_SESSION['nombre']=$result->nombre;
                $_SESSION['email']=$result->correo;

                $marcados = $usuarios->listarmarcados($result->idusuarios);

                $valores=array();

                while ($per = $marcados->fetch_object())
                {
                    array_push($valores, $per->idpermiso);
                }

            } else {
                echo "null";
            }
            
        } else {
            echo "null";
        }
        break;
    default:
        echo "OPCION NO EXISTE";
        break;
}