<?php
require_once "../modelos/Formadepago.php";

$formadepago = new Formadepago();

$idformadepago = isset($_POST["idformadepago"])? limpiarCadena($_POST["idformadepago"]):"";
$descripcion = isset($_POST["descripcion"])? limpiarCadena($_POST["descripcion"]):"";

switch ($_GET["op"]) {
    case 'agregaryeditar':
        if (empty($idformadepago)) {
            $result=$formadepago->agregar($descripcion);
            echo $result;
        } else {
            $result=$formadepago->editar($idformadepago,$descripcion);
            echo $result;
        }
        
        break;
    
    case 'mostrar':
        $result=$formadepago->mostrar($idformadepago);
        echo json_encode($result);
        break;

    case 'listar':
        $result=$formadepago->listar();
        $data=array();

        while ($reg=$result->fetch_object()) {
            $data[]=array(
                "0" => $reg->idformadepago,
                "1" => $reg->descripcion
            );
        }
        $results = array(
            "sEcho"=>1,
            "iTotalRecords"=>count($data),
            "iTotalDisplayRecords"=>count($data),
            "aaData"=>$data);
        echo json_encode($results);

        break;

    default:
        echo "OPCION NO ENCONTRADA";
        break;
}