<?php
require_once '../modelos/Movimiento.php';

$movimiento = new Movimiento();

$idmovimiento = isset($_POST['idmovimiento'])?limpiarcadena($_POST['idmovimiento']):"";
$descripcion = isset($_POST['descripcion'])?limpiarcadena($_POST['descripcion']):"";

switch ($_GET["op"]) {
    case 'agregaryeditar':
        if (empty($idmovimiento)) {
            $result = $movimiento->agregar($descripcion);
            echo $result;
        } else {
            $result = $movimiento->editar($idmovimiento, $descripcion);
            echo $result;
        }

        break;
    case 'mostrar':
        $result = $movimiento->mostrar($idmovimiento);
        echo json_encode($result);
        break;
    case 'listar':
        $result = $movimiento->listar();

        $data = array();

        while ($reg=$result->fetch_object()) {
            $data[]=array(
                "0" => $reg->idmovimiento,
                "1" => $reg->descripcion
            );
        }

        $results = array(
            "sEcho"=>1,
            "iTotalRecords"=>count($data),
            "iTotalDisplayRecords"=>count($data),
            "aaData"=>$data);
        echo json_encode($results);
        break;
    case 'select':
        $result = $movimiento->listar();

        $data = array();

        while ($reg=$result->fetch_object()) {
            $data[]=array(
                "0" => $reg->idmovimiento,
                "1" => $reg->descripcion
            );
        }

        echo json_encode($data);
        break;
    default:
        echo "OPCION NO ENCONTRADA";
        break;
}