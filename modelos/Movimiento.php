<?php
require "../config/Conexion.php";

class Movimiento
{
    public function __construct()
    {
        # code...
    }

    public function agregar($descripcion)
    {
        $sql = "INSERT INTO `bd_apl2020`.`movimiento`
        (`descripcion`)
        VALUES
        ('$descripcion')";
     
        return ejecutarConsulta($sql);
    }

    public function editar($idmovimiento, $descripcion)
    {
        $sql = "UPDATE `bd_apl2020`.`movimiento`
        SET
        `descripcion` = $descripcion
        WHERE `idmovimiento` = $idmovimiento;
        ";

        return ejecutarConsulta($sql);
    }

    public function mostrar($idmovimiento)
    {
        $sql = "SELECT `idmovimiento`,
        `descripcion`
        FROM `bd_apl2020`.`movimiento` WHERE `idmovimiento` = $idmovimiento;";

        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar()
    {
        $sql = "SELECT `idmovimiento`,
        `descripcion`
        FROM `bd_apl2020`.`movimiento`;";
        
        return ejecutarConsulta($sql);        
    }
}
