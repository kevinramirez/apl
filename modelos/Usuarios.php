<?php
require "../config/Conexion.php";

class Usuarios
{
    public function __construct()
    {
        # code...
    }

    public function agregar($nombre,$apellidos,$correo,$password,$role)
    {
        $sql = "INSERT INTO `bd_apl2020`.`usuarios`
        (`nombre`,
        `apellidos`,
        `correo`,
        `password`,
        `role`)
        VALUES
        ('$nombre',
        '$apellidos',
        '$correo',
        '$password',
        '$role');";

        return ejecutarConsulta($sql);
    }

    public function editar($idusuarios,$nombre,$apellidos,$correo,$role)
    {
        $sql = "UPDATE `bd_apl2020`.`usuarios`
        SET
        `nombre` = '$nombre',
        `apellidos` = '$apellidos',
        `correo` = '$correo',
        `role` = '$role'
        WHERE `idusuarios` = '$idusuarios'";

        return ejecutarConsulta($sql);
    }

    public function mostrar($idusuarios)
    {
        $sql = "SELECT
        `idusuarios`,
        `nombre`,
        `apellidos`,
        `correo`,
        `role` FROM `bd_apl2020`.`usuarios` WHERE `idusuarios` = '$idusuarios'";

        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar()
    {
        $sql = "SELECT
        `idusuarios`,
        `nombre`,
        `apellidos`,
        `correo`,
        `status`,
        `role` FROM `bd_apl2020`.`usuarios`";

        return ejecutarConsulta($sql);
    }

    public function desactivar($idusuarios)
    {
        $sql="UPDATE `bd_apl2020`.`usuarios` SET `status`=0 WHERE `idusuarios`='$idusuarios'";
        return ejecutarConsulta($sql);
    }

    public function activar($idusuarios)
    {
        $sql="UPDATE `bd_apl2020`.`usuarios` SET `status`=1 WHERE `idusuarios`='$idusuarios'";
        return ejecutarConsulta($sql);
    }

    public function verificar($correo)
    {
        $sql="SELECT
        `idusuarios`,
        `nombre`,
        `apellidos`,
        `correo`,
        `password`,
        `status`,
        `role` FROM `bd_apl2020`.`usuarios` WHERE `correo` = '$correo' AND `status` = '1'"; 
        return ejecutarConsulta($sql);
    }
}
