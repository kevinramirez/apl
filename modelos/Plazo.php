<?php
require "../config/Conexion.php";

class Plazo
{
    public function __construct()
    {
        # code...
    }

    public function agregar($descripcion)
    {
        $sql = "INSERT INTO `bd_apl2020`.`plazo`
        (`descripcion`)
        VALUES
        ('$descripcion');";

        return ejecutarConsulta($sql);
    }

    public function editar($idplazo,$descripcion)
    {
        $sql = "UPDATE `bd_apl2020`.`plazo`
        SET
        `idplazo` = $idplazo,
        `descripcion` = $descripcion
        WHERE `idplazo` = $idplazo";

        return ejecutarConsulta($sql);
    }

    public function mostrar($idplazo)
    {
        $sql = "SELECT
        `idplazo`,
		`descripcion`
        FROM `bd_apl2020`.`plazo` WHERE `idplazo` = '$idplazo'";

        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar()
    {
        $sql = "SELECT
        `idplazo`,
        `descripcion`
        FROM `bd_apl2020`.`plazo`";

        return ejecutarConsulta($sql);
    }
}