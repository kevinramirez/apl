<?php
require "../config/Conexion.php";

class Clientes
{
    public function __construct()
    {
        # code...
    }

    public function agregar($idmovimiento,$persona,$celular,$idplazo,$idformadepago,$nombre,$apellidop,$apellidom,$rfc,$fechanac,$apoderadolegal,$rfcempresa,$calle,$numexterior,$numinterior,$colonia,$ciudad,$municipio,$estado,$codigopostal,$numcontacto,$burodecredito,$tarjetadecredito,$creditohipotecario,$creditoautomotriz,$autorizo1,$autorizo2,$lugarnac,$tipoidentificacion,$folioidentificacion,$sexo,$curp,$correo,$confirmarcorreo,$banco,$telefono,$referencias,$entrecalle,$ycalle,$ocupacion,$empresa,$telefonoempresa,$antiguedad)
    {
        $sql = "INSERT INTO `bd_apl2020`.`clientes`
        (`idmovimiento`,
        `persona`,
        `celular`,
        `idplazo`,
        `idformadepago`,
        `nombre`,
        `apellidop`,
        `apellidom`,
        `rfc`,
        `fechanac`,
        `apoderadolegal`,
        `rfcempresa`,
        `calle`,
        `numexterior`,
        `numinterior`,
        `colonia`,
        `ciudad`,
        `municipio`,
        `estado`,
        `codigopostal`,
        `numcontacto`,
        `burodecredito`,
        `tarjetadecredito`,
        `creditohipotecario`,
        `creditoautomotriz`,
        `autorizo1`,
        `autorizo2`,
        `lugarnac`,
        `tipoidentificacion`,
        `folioidentificacion`,
        `sexo`,
        `curp`,
        `correo`,
        `confirmarcorreo`,
        `banco`,
        `telefono`,
        `referencias`,
        `entrecalle`,
        `ycalle`,
        `ocupacion`,
        `empresa`,
        `telefonoempresa`,
        `antiguedad`)
        VALUES
        ('$idmovimiento',
        '$persona',
        '$celular',
        '$idplazo',
        '$idformadepago',
        '$nombre',
        '$apellidop',
        '$apellidom',
        '$rfc',
        '$fechanac',
        '$apoderadolegal',
        '$rfcempresa',
        '$calle',
        '$numexterior',
        '$numinterior',
        '$colonia',
        '$ciudad',
        '$municipio',
        '$estado',
        '$codigopostal',
        '$numcontacto',
        '$burodecredito',
        '$tarjetadecredito',
        '$creditohipotecario',
        '$creditoautomotriz',
        '$autorizo1',
        '$autorizo2',
        '$lugarnac',
        '$tipoidentificacion',
        '$folioidentificacion',
        '$sexo',
        '$curp',
        '$correo',
        '$confirmarcorreo',
        '$banco',
        '$telefono',
        '$referencias',
        '$entrecalle',
        '$ycalle',
        '$ocupacion',
        '$empresa',
        '$telefonoempresa',
        '$antiguedad');";

        return ejecutarConsulta($sql);
    }

    public function editar($idclientes,$idmovimiento,$persona,$celular,$idplazo,$idformadepago,$nombre,$apellidop,$apellidom,$rfc,$fechanac,$apoderadolegal,$rfcempresa,$calle,$numexterior,$numinterior,$colonia,$ciudad,$municipio,$estado,$codigopostal,$numcontacto,$burodecredito,$tarjetadecredito,$creditohipotecario,$creditoautomotriz,$autorizo1,$autorizo2,$lugarnac,$tipoidentificacion,$folioidentificacion,$sexo,$curp,$correo,$confirmarcorreo,$banco,$telefono,$referencias,$entrecalle,$ycalle,$ocupacion,$empresa,$telefonoempresa,$antiguedad)
    {
        $sql = "UPDATE `bd_apl2020`.`clientes`
        SET
        `idmovimiento` = '$idmovimiento',
        `persona` = '$persona',
        `celular` = '$celular',
        `idplazo` = '$idplazo',
        `idformadepago` = '$idformadepago',
        `nombre` = '$nombre',
        `apellidop` = '$apellidop',
        `apellidom` = '$apellidom',
        `rfc` = '$rfc',
        `fechanac` = '$fechanac',
        `apoderadolegal` = '$apoderadolegal',
        `rfcempresa` = '$rfcempresa',
        `calle` = '$calle',
        `numexterior` = '$numexterior',
        `numinterior` = '$numinterior',
        `colonia` = '$colonia',
        `ciudad` = '$ciudad',
        `municipio` = '$municipio',
        `estado` = '$estado',
        `codigopostal` = '$codigopostal',
        `numcontacto` = '$numcontacto',
        `burodecredito` = '$burodecredito',
        `tarjetadecredito` = '$tarjetadecredito',
        `creditohipotecario` = '$creditohipotecario',
        `creditoautomotriz` = '$creditoautomotriz',
        `autorizo1` = '$autorizo1',
        `autorizo2` = '$autorizo2',
        `lugarnac` = '$lugarnac',
        `tipoidentificacion` = '$tipoidentificacion',
        `folioidentificacion` = '$folioidentificacion',
        `sexo` = '$sexo',
        `curp` = '$curp',
        `correo` = '$correo',
        `confirmarcorreo` = '$confirmarcorreo',
        `banco` = '$banco',
        `telefono` = '$telefono',
        `referencias` = '$referencias',
        `entrecalle` = '$entrecalle',
        `ycalle` = '$ycalle',
        `ocupacion` = '$ocupacion',
        `empresa` = '$empresa',
        `telefonoempresa` = '$telefonoempresa',
        `antiguedad` = '$antiguedad'
        WHERE `idclientes` = '$idclientes'";

        return ejecutarConsulta($sql);
    }

    public function mostrar($idclientes)
    {
        $sql = "SELECT 
            `clientes`.`idclientes`,
            `movimiento`.`descripcion` `movimiento`,
            `clientes`.`persona`,
            `clientes`.`celular` ,
            `plazo`.`descripcion` `plazo`,
            `formadepago`.`descripcion` `formadepago`,
            `clientes`.`nombre`,
            `clientes`.`apellidop`,
            `clientes`.`apellidom`,
            `clientes`.`rfc`,
            `clientes`.`fechanac`,
            `clientes`.`apoderadolegal`,
            `clientes`.`rfcempresa`,
            `clientes`.`calle`,
            `clientes`.`numexterior`,
            `clientes`.`numinterior`,
            `clientes`.`colonia`,
            `clientes`.`ciudad`,
            `clientes`.`municipio`,
            `clientes`.`estado`,
            `clientes`.`codigopostal`,
            `clientes`.`numcontacto`,
            `clientes`.`burodecredito`,
            `clientes`.`tarjetadecredito`,
            `clientes`.`creditohipotecario`,
            `clientes`.`creditoautomotriz`,
            `clientes`.`autorizo1`,
            `clientes`.`autorizo2`,
            `clientes`.`lugarnac`,
            `clientes`.`tipoidentificacion`,
            `clientes`.`folioidentificacion`,
            `clientes`.`sexo`,
            `clientes`.`curp`,
            `clientes`.`correo`,
            `clientes`.`confirmarcorreo`,
            `clientes`.`banco`,
            `clientes`.`telefono`,
            `clientes`.`referencias`,
            `clientes`.`entrecalle`,
            `clientes`.`ycalle`,
            `clientes`.`ocupacion`,
            `clientes`.`empresa`,
            `clientes`.`telefonoempresa`,
            `clientes`.`antiguedad`,
            `clientes`.`modified`,
            `clientes`.`created`
        FROM
            `bd_apl2020`.`clientes`
        LEFT JOIN
            `movimiento` ON `movimiento`.`idmovimiento` = `clientes`.`idmovimiento`
        LEFT JOIN
            `plazo` ON `plazo`.`idplazo` = `clientes`.`idplazo`
        LEFT JOIN
            `formadepago` ON `formadepago`.`idformadepago` = `clientes`.`idformadepago`
        WHERE `clientes`.`idclientes` = '$idclientes'";

        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar()
    {
        $sql = "SELECT
        `idclientes`,
        `persona`,
        `celular`,
        `nombre`,
        `apellidop`,
        `apellidom`,
        `rfc`
        FROM `bd_apl2020`.`clientes`";

        return ejecutarConsulta($sql);
    }
}
