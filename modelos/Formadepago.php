<?php
require "../config/Conexion.php";

class Formadepago
{
    public function __construct()
    {
        # code...
    }

    public function agregar($descripcion)
    {
        $sql = "INSERT INTO `bd_apl2020`.`formadepago`
		(`descripcion`)
        VALUES
        ('$descripcion');";

        return ejecutarConsulta($sql);
    }

    public function editar($idformadepago,$descripcion)
    {
        $sql = "UPDATE `bd_apl2020`.`formadepago`
        SET
        `descripcion` = $descripcion
        WHERE `idformadepago` = $idformadepago";

        return ejecutarConsulta($sql);
    }

    public function mostrar($idformadepago)
    {
        $sql = "SELECT
        `idformadepago`,
        `descripcion`
        FROM `bd_apl2020`.`formadepago` WHERE `idformadepago` = '$idformadepago'";

        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar()
    {
        $sql = "SELECT
        `idformadepago`,
        `descripcion`
        FROM `bd_apl2020`.`formadepago`";

        return ejecutarConsulta($sql);
    }
}
