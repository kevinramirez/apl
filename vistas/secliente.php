<?php require 'header.php'; ?>
<link rel="stylesheet" type="text/css" href="../public/assets/css/forms/switches.css">
<link rel="stylesheet" type="text/css" href="../public/assets/css/forms/theme-checkbox-radio.css">
<link href="../public/assets/css/tables/table-basic.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../public/plugins/bootstrap-select/bootstrap-select.min.css">
<style>
.btn-outline-primary:hover {
    color: #fff !important;
    background-color: #1b55e2;
}
</style>
<div class="layout-px-spacing">
<div class="row layout-top-spacing">
<form id="form" method="POST" class="col-12 col-lg-12">
    <div class="col-lg-12 col-12  layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">                                
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Tipo Movimiento</h4>
                    </div>     
                </div>
            </div>
            <div class="widget-content widget-content-area">

                <div class="row">

                    <div class="col-xl-4">
                        <div class="form-group">
                            <label for="movimiento">Movimiento</label>
                            <input type="hidden" id="idclientes" name="idclientes">
                            <select class="selectpicker form-control" id="movimiento" name="movimiento" required="">
                                <option value="1">Nueva</option>
                                <option value="2">Migración normal</option>
                                <option value="3">Migración mixta</option>
                                <option value="4">Portabilidad</option>
                                <option value="5">Renovación</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group">
                            <label for="persona">Persona</label>
                            <div class="row">

                                <div class="col-6">
                                    <div class="n-chk">
                                        <label class="new-control new-radio radio-primary">
                                            <input type="radio" class="new-control-input" name="persona" value="Fisica" checked>
                                            <span class="new-control-indicator"></span>Fisica
                                        </label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="n-chk">
                                        <label class="new-control new-radio radio-primary">
                                            <input type="radio" class="new-control-input" name="persona" value="Moral">
                                            <span class="new-control-indicator"></span>Moral
                                        </label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group">
                            <label for="numero_celular">Número Celular</label>
                            <input id="numero_celular" name="numero_celular" class="form-control form-control-sm" type="text" maxlength="15" required="">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-12  layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">                                
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Tipo evaluación</h4>
                    </div>     
                </div>
            </div>
            <div class="widget-content widget-content-area">

                <div class="row">
                    
                    <div class="col-xl-4">
                        <div class="form-group">
                            <label for="plazo">Plazo</label>
                            <select class="selectpicker form-control form-control-sm" name="plazo" id="plazo" required="">
                                <option value="1">Libre</option>
                                <option value="2">12 Meses</option>
                                <option value="3">18 Meses</option>
                                <option value="4">24 Meses</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group">
                            <label for="forma_pago">Forma de Pago</label>
                            <select class="selectpicker form-control form-control-sm" name="forma_pago" id="forma_pago" required>
                                <option value="1">Tarjeta de credito</option>
                                <option value="2">Efectivo</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-4"></div>

                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-12  layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">                                
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Datos personales</h4>
                    </div>     
                </div>
            </div>
            <div class="widget-content widget-content-area">

                <div class="row">
                    
                    <div class="col-xl-3">
                        <div class="form-group">
                            <label for="nombre">Nombre o Razón Social</label>
                            <input id="nombre" name="nombre" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="form-group">
                            <label for="apellido_paterno">Apellido Paterno</label>
                            <input id="apellido_paterno" name="apellido_paterno" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="form-group">
                            <label for="apellido_materno">Apellido Materno</label>
                            <input id="apellido_materno" name="apellido_materno" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
					<div class="col-xl-3">
                        <div class="form-group">
                            <label for="fechanac">Fecha de nacimiento o constitución</label>
                            <input id="fechanac" name="fechanac" class="form-control form-control-sm" type="text" placeholder="aaaa-mm-dd">
                        </div>
                    </div>

                </div>
                <?php /*
                <div class="row">

					<div class="col-xl-4">
                        <div class="form-group">
                            <label for="rfc">RFC</label>
                            <input id="rfc" name="rfc" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group">
                            <label for="apoderado_legal">Apoderado legal</label>
                            <input id="apoderado_legal" name="apoderado_legal" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group">
                            <label for="rfc_empresa">RFC empresa</label>
                            <input id="rfc_empresa" name="rfc_empresa" class="form-control form-control-sm" type="text">
                        </div>
                    </div>

                </div> */ ?>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-12  layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">                                
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Domicilio personal</h4>
                    </div>     
                </div>
            </div>
            <div class="widget-content widget-content-area">

                <div class="row">

					<div class="col-xl-3">
                        <div class="form-group">
                            <label for="calle">Calle</label>
                            <input id="calle" name="calle" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="form-group">
                            <label for="numero_exterior">Numero exterior</label>
                            <input id="numero_exterior" name="numero_exterior" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="form-group">
                            <label for="numero_interior">Numero interior</label>
                            <input id="numero_interior" name="numero_interior" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
					<div class="col-xl-3">
                        <div class="form-group">
                            <label for="colonia">Colonia</label>
                            <input id="colonia" name="colonia" class="form-control form-control-sm" type="text">
                        </div>
                    </div>

                </div>
                <div class="row">

					<div class="col-xl-3">
                        <div class="form-group">
                            <label for="ciudad">Ciudad</label>
                            <input id="ciudad" name="ciudad" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
					<div class="col-xl-3">
                        <div class="form-group">
                            <label for="municipio">Municipio</label>
                            <input id="municipio" name="municipio" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
					<div class="col-xl-3">
                        <div class="form-group">
                            <label for="estado">Estado</label>
                            <select name="estado" class="selectpicker form-control form-control-sm" data-live-search="true" id="estado" required="">
                                <option value="">Seleccione estado...</option>
                                <option value="Aguascalientes">Aguascalientes</option>
                                <option value="Baja California">Baja California</option>
                                <option value="Baja California Sur">Baja California Sur</option>
                                <option value="Campeche">Campeche</option>
                                <option value="Chiapas">Chiapas</option>
                                <option value="Chihuahua">Chihuahua</option>
                                <option value="CDMX">Ciudad de México</option>
                                <option value="Coahuila">Coahuila</option>
                                <option value="Colima">Colima</option>
                                <option value="Durango">Durango</option>
                                <option value="Estado de México">Estado de México</option>
                                <option value="Guanajuato">Guanajuato</option>
                                <option value="Guerrero">Guerrero</option>
                                <option value="Hidalgo">Hidalgo</option>
                                <option value="Jalisco">Jalisco</option>
                                <option value="Michoacán">Michoacán</option>
                                <option value="Morelos">Morelos</option>
                                <option value="Nayarit">Nayarit</option>
                                <option value="Nuevo León">Nuevo León</option>
                                <option value="Oaxaca">Oaxaca</option>
                                <option value="Puebla">Puebla</option>
                                <option value="Querétaro">Querétaro</option>
                                <option value="Quintana Roo">Quintana Roo</option>
                                <option value="San Luis Potosí">San Luis Potosí</option>
                                <option value="Sinaloa">Sinaloa</option>
                                <option value="Sonora">Sonora</option>
                                <option value="Tabasco">Tabasco</option>
                                <option value="Tamaulipas">Tamaulipas</option>
                                <option value="Tlaxcala">Tlaxcala</option>
                                <option value="Veracruz">Veracruz</option>
                                <option value="Yucatán">Yucatán</option>
                                <option value="Zacatecas">Zacatecas</option>
                            </select>
                        </div>
                    </div>

					<div class="col-xl-3">
                        <div class="form-group">
                            <label for="codigo_postal">Código Postal</label>
                            <input id="codigo_postal" name="codigo_postal" class="form-control form-control-sm" type="text">
                        </div>
                    </div>

                </div>
                <hr>
                <div class="form-group row mb-4">
                    <label for="numcontacto" class="col-sm-4 col-form-label col-form-label-sm">Numero de contacto del cliente:</label>
                    <div class="col-sm-3">
                        <input type="tel" class="form-control form-control-sm" name="numcontacto" id="numcontacto" maxlength="10">
                    </div>
                </div>

            </div>
        </div>
    </div>
<!--
    <div class="col-lg-12 col-12  layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">                                
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Informacion crediticia</h4>
                    </div>     
                </div>
            </div>
            <div class="widget-content widget-content-area">

                <div class="row">

                    <div class="col-xl-5">
                        <p class="note-description">¿Acepta cliente revisarlo en Buro de Credito?</p>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <label class="switch s-outline s-outline-primary mb-4 mr-2">
                            <input type="checkbox" name="buro_de_credito" id="buro_de_credito">
                            <span class="slider round"></span>
                        </label>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xl-5">
                        <p class="note-description">¿Cuenta con Tarjeta de Credito?</p>
                        <p style="font-size:11px;">Unicamente tarjetas de credito bancarias (tarjetas de servicio no estan consideradas)</p>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <label class="switch s-outline s-outline-primary mb-4 mr-2">
                            <input type="checkbox" name="tarjeta_credito" id="tarjeta_credito">
                            <span class="slider round"></span>
                        </label>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xl-5">
                        <p class="note-description">¿Ha ejercido un credito Hipotecario?</p>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <label class="switch s-outline s-outline-primary mb-4 mr-2">
                            <input type="checkbox" name="credito_hipotecario" id="credito_hipotecario">
                            <span class="slider round"></span>
                        </label>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xl-5">
                        <p class="note-description">¿Ha ejercido un credito automotriz en los ultimos en los ultimos 24 meses?</p>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <label class="switch s-outline s-outline-primary mb-4 mr-2">
                            <input type="checkbox" name="credito_automotriz" id="credito_automotriz">
                            <span class="slider round"></span>
                        </label>
                    </div>

                </div>
                <hr>
                <div class="row">
                    <p class="mx-3"><label class="new-control new-checkbox new-checkbox-text checkbox-primary mr-0">
                        <input type="checkbox" class="new-control-input" name="autorizo1" id="autorizo1">
                        <span class="new-control-indicator"></span><span class="new-chk-content">Autorizo</span>
                    </label>
                    <span>a Radiomovil Dipsa S.A. de C.V. (telcel) para que realice la verificacion de datos con la informacion proporcionada (vista domiciliaria y/o confirmacion telefonica), a traves del personal de telcel y/o de un tercero de su eleccion</span></p>
                </div>
                <hr>
                <div class="row">
                    <p class="mx-3"><label class="new-control new-checkbox new-checkbox-text checkbox-primary mr-0">
                        <input type="checkbox" class="new-control-input" name="autorizo2" id="autorizo2">
                        <span class="new-control-indicator"></span><span class="new-chk-content">Autorizo</span>
                    </label>
                    <span>a Radiomovil Dipsa S.A. de C.V. (telcel) y a Fianzas Guardiana Inbursa S.A. Grupo Financiero Inbursa para que individual e indistintamente lleve(n) a cabo de investigaciones y monitoreos periodicos sobre el comportamiento crediticio de mi como persona fisico y/o de la empresa que represento como persona moral. Asi mismo, DECLARO que conozco la naturaleza y alcance legal de la informacion que se solicitara y ACEPTO que este documento quede bajo propiedad de Telcel para efecto del cumplimiento con lo dispuesto en la Ley para Regular a las Sociedades de informacion crediticia y demas leyes aplicables. CONSIENTO que esta autorizacion se encuentre vigente por un periodo de 3 años contados a partir de su fecha de expedicion y, en todo caso, durante el tiempo que mantega una relacion juridica con TELCEL</span></p>
                </div>
                <div class="row">
                    <div class="col-12 text-right">
                        <a href="javascript:void(0);" class="btn btn-outline-primary mb-2 mr-2">PREPARAR EVALUACION</a>
                        <a href="javascript:void(0);" class="btn btn-outline-primary mb-2">Captura de tickets de pago para deudores</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-12  layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">                                
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4 class="text-center">Complemento de datos de cliente para crear folio sisact</h4>
                        <h4>Datos personales</h4>
                    </div>     
                </div>
            </div>
            <div class="widget-content widget-content-area">

                <div class="form-group row mb-2">
                    <label for="lugar_nac" class="col-sm-2 col-form-label col-form-label-sm">Lugar de nacimiento:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" id="lugar_nac" name="lugar_nac">
                    </div>
                </div>

                <div class="form-group row mb-2">
                    <label for="tipo_identificacion" class="col-sm-2 col-form-label col-form-label-sm">Tipo identificacion:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" id="tipo_identificacion" name="tipo_identificacion">
                    </div>
                </div>

                <div class="form-group row mb-2">
                    <label for="folio_identificacion" class="col-sm-2 col-form-label col-form-label-sm">Folio identificacion:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" id="folio_identificacion" name="folio_identificacion">
                    </div>
                </div>

                <div class="form-group row mb-2">
                    <label for="sexo" class="col-sm-2 col-form-label col-form-label-sm">Sexo:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" id="sexo" name="sexo">
                    </div>
                </div>

                <div class="form-group row mb-2">
                    <label for="curp" class="col-sm-2 col-form-label col-form-label-sm">CURP:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" id="curp" name="curp">
                    </div>
                </div>
                <hr>
                <div class="form-group row mb-2">
                    <label for="correo" class="col-sm-2 col-form-label col-form-label-sm">Correo electronico:</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm" id="correo" name="correo">
                    </div>
                </div>

                <div class="form-group row mb-2">
                    <label for="confirmar_correo" class="col-sm-2 col-form-label col-form-label-sm">Confirmar correo electronico:</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm" id="confirmar_correo" name="confirmar_correo">
                    </div>
                </div>
                <p class="col-form-label col-form-label-sm">¡¡¡Es importante la captura del correo electronico, permitira enviar una solicitud de alta para factura electronica!!!</p>
                <hr>
                <div class="form-group row mb-2">
                    <label for="banco" class="col-sm-1 col-form-label col-form-label-sm">Banco:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" id="banco" name="banco">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-12  layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">                                
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Domicilio personal</h4>
                    </div>     
                </div>
            </div>
            <div class="widget-content widget-content-area">

                <div class="form-group row mb-2">
                    <label for="telefono" class="col-sm-3 col-form-label col-form-label-sm">Telefono:</label>
                    <div class="col-sm-2">
                        <input type="tel" class="form-control form-control-sm" id="telefono" name="telefono">
                    </div>
                </div>

                <div class="form-group row mb-2">
                    <label for="referencias_dom" class="col-sm-3 col-form-label col-form-label-sm">Referencias del domicilio:</label>
                    <div class="col-sm-5">
                    <textarea class="form-control form-control-sm" id="referencias_dom" name="referencias_dom" rows="3"></textarea>
                    </div>
                </div>

                <div class="form-group row mb-2">
                    <label for="entre_calle" class="col-sm-3 col-form-label col-form-label-sm">Entre calle:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" id="entre_calle" name="entre_calle">
                    </div>
                    <label for="y_calle" class="col-sm-1 col-form-label col-form-label-sm text-center">y:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" id="y_calle" name="y_calle">
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">                                
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Empleo</h4>
                    </div>     
                </div>
            </div>
            <div class="widget-content widget-content-area">

                <div class="row">
                    
                    <div class="col-xl-4">
                        <div class="form-group">
                            <label for="ocupacion">Ocupación o Puesto</label>
                            <input id="ocupacion" name="ocupacion" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group">
                            <label for="empresa">Empresa</label>
                            <input id="empresa" name="empresa" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
                    <div class="col-xl-4"></div>

                </div>
                <div class="row">

                    <div class="col-xl-4">
                        <div class="form-group">
                            <label for="telefono_empresa">Teléfono Empresa</label>
                            <input id="telefono_empresa" name="telefono_empresa" class="form-control form-control-sm" type="text">
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group">
                            <label for="antiguedad_empresa">Antigüedad en la Empresa</label>
                            <input id="antiguedad_empresa" name="antiguedad_empresa" class="form-control form-control-sm" type="text">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
-->
    <div class="col-lg-12 col-12  layout-spacing">
        <div class="statbox widget box box-shadow">
            <!-- <div class="widget-header">                                
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Referencias</h4>
                    </div>     
                </div>
            </div> -->
            <div class="widget-content widget-content-area">
                <!-- <div class="table-responsive">
                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Telefono</th>
                                <th>Tel. Oficina</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">1</td>
                                <td>John</td>
                                <td>Doe</td>
                                <td>0000000000</td>
                                <td></td>
                                <td class="text-center">
                                    <ul class="table-controls">
                                        <li><a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></a></li>
                                        <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a></li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">2</td>
                                <td>Andy</td>
                                <td>King</td>
                                <td>0000000000</td>
                                <td></td>
                                <td class="text-center">
                                    <ul class="table-controls">
                                        <li><a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></a></li>
                                        <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a></li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">3</td>
                                <td>Lisa</td>
                                <td>Doe</td>
                                <td>0000000000</td>
                                <td></td>
                                <td class="text-center">
                                    <ul class="table-controls">
                                        <li><a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></a></li>
                                        <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a></li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">4</td>
                                <td>Vincent</td>
                                <td>Carpenter</td>
                                <td>0000000000</td>
                                <td></td>
                                <td class="text-center">
                                    <ul class="table-controls">
                                        <li><a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></a></li>
                                        <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a></li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> -->
                <div class="row">
                    <div class="col-12 text-right">
                        <button class="btn btn-outline-primary my-2 mr-2" type="submit" id="btnGuardar">GENERAR FOLIO SISACT</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="col-lg-12 col-12  layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="text-right">
                <div class="widget-content widget-content-area">
                <a href="javascript:void(0);" class="btn btn-outline-primary my-2 mr-2">NUEVA EVALUACION</a>
                <a href="javascript:void(0);" class="btn btn-outline-primary my-2 mr-2">ABRIR SISACT WEB</a>
                <a href="javascript:void(0);" class="btn btn-outline-primary my-2 mr-2">ver detalle de error</a>
                </div>
            </div>
        </div>
    </div> -->
</form>
</div>
</div>
<?php require 'footer.php'; ?>
<script src="../public/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="../public/plugins/table/datatable/datatables.js"></script>
<script src="../public/plugins/input-mask/jquery.inputmask.bundle.min.js"></script>
<script src="../public/plugins/input-mask/input-mask.js"></script>
<script src="scripts/secliente.js"></script>
<script>
    checkall('todoAll', 'todochkbox');
    $('[data-toggle="tooltip"]').tooltip()
</script>