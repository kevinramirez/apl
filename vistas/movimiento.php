<?php require 'header.php'; ?>
    <link rel="stylesheet" type="text/css" href="../public/plugins/table/datatable/datatables.css">
    <link rel="stylesheet" type="text/css" href="../public/plugins/table/datatable/dt-global_style.css">
<div class="container">
    <div class="container">
        <div class="row layout-top-spacing">
            <div class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4 class="float-left">tipo movimiento</h4>
                                <button class="btn btn-sm btn-primary mt-3 float-right">Nuevo</button>
                            </div>                 
                        </div>
                    </div> <!-- .widget-header -->
                    <div class="widget-content widget-content-area">
                        <form id="form" class="simple-example was-validated" method="POST">
                            <div class="form-row">
                                <div class="col-md-12 mb-4">
                                    <label for="descripcion">Descripcion movimiento</label>
                                    <input type="hidden" name="idmovimiento" id="idmovimiento">
                                    <input type="text" class="form-control" id="descripcion" name="descripcion" required="">
                                    <div class="valid-feedback">
                                        ¡Bien!
                                    </div>
                                    <div class="invalid-feedback">
                                        Introducir movimiento
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-sm submit-fn mt-2" type="submit">Guardar</button>
                        </form>
                        <table id="tblmovimiento" class="table" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Num</th>
                                    <th>Movimiento</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div> <!-- .widget-content -->
               </div> <!-- .statbox -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
    </div> <!-- .container -->
</div> <!-- .container -->
<?php require 'footer.php'; ?>
<script src="../public/plugins/table/datatable/datatables.js"></script>
<script src="scripts/movimiento.js"></script>