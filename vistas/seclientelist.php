<?php require 'header.php'; ?>
<link rel="stylesheet" type="text/css" href="../public/plugins/table/datatable/datatables.css">
    <link rel="stylesheet" type="text/css" href="../public/plugins/table/datatable/dt-global_style.css">

<div class="layout-px-spacing">
    <div class="row layout-top-spacing">
        <div class="col-lg-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4 class="float-left">Clientes</h4>
                            <a href="secliente.php" class="btn btn-sm btn-outline-primary mt-3 float-right">Registrar</a>
                        </div>                 
                    </div>
                </div> <!-- .widget-header -->
                <div class="widget-content widget-content-area">
                    <table id="tblsecliente" class="table" style="width:100%">
                        <thead>
                            <tr>
                                <th>Num</th>
                                <th>Tipo persona</th>
                                <th>Celular</th>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>RFC</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div> <!-- .widget-content -->
            </div> <!-- .statbox -->
        </div> <!-- .col-lg-12 -->
    </div> <!-- .row -->
</div> <!-- .layout-px-spacing -->
<?php require 'footer.php'; ?>
<script src="../public/plugins/table/datatable/datatables.js"></script>
<script src="scripts/secliente.js"></script>