<?php require 'header.php'; ?>
<div class="container">
    <div class="container">
        <div class="row layout-top-spacing">
            <div class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4 class="float-left">Usuarios</h4>
                                <button class="btn btn-sm btn-primary mt-3 float-right">Nuevo</button>
                            </div>                 
                        </div>
                    </div> <!-- .widget-header -->
                    <div class="widget-content widget-content-area">
                        <form id="form" class="simple-example was-validated" method="POST">
                            <div class="form-row">
                                <div class="col-md-6 mb-4">
                                    <label for="nombre">Nombre</label>
                                    <input type="hidden" name="idusuarios" id="idusuarios">
                                    <input type="text" class="form-control" id="nombre" name="nombre" required="">
                                    <div class="valid-feedback">
                                        ¡Bien!
                                    </div>
                                    <div class="invalid-feedback">
                                        Introducir nombre
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <label for="apellidos">Apellidos</label>
                                    <input type="text" class="form-control" id="apellidos" name="apellidos" required="">
                                    <div class="valid-feedback">
                                        ¡Bien!
                                    </div>
                                    <div class="invalid-feedback">
                                        Introducir apellidos
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4">
                                    <label for="correo">Correo electronico</label>
                                    <input type="email" class="form-control" id="correo" name="correo" required="">
                                    <div class="valid-feedback">
                                        ¡Bien!
                                    </div>
                                    <div class="invalid-feedback">
                                        Introducir correo
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="password">Contraseña</label>
                                    <input type="password" class="form-control" id="password" name="password" required="">
                                    <div class="valid-feedback">
                                        ¡Bien!
                                    </div>
                                    <div class="invalid-feedback">
                                        Introducir contraseña
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="role">Role</label>
                                    <input type="text" class="form-control" id="role" name="role" required="">
                                    <div class="valid-feedback">
                                        ¡Bien!
                                    </div>
                                    <div class="invalid-feedback">
                                        Introducir role
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-sm submit-fn mt-2" type="submit">Guardar</button>
                        </form>
                        <table id="tblusuarios" class="table mt-3" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Num</th>
                                    <th>Nombre</th>
                                    <th>Apellidos</th>
                                    <th>Correo</th>
                                    <th>Role</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div> <!-- .widget-content -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php require 'footer.php'; ?>
<script src="../public/plugins/table/datatable/datatables.js"></script>
<script src="scripts/usuarios.js"></script>