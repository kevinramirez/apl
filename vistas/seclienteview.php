<?php require 'header.php'; ?>
<div class="container">
    <div class="container">
        <!-- <div id="navSection" data-spy="affix" class="nav  sidenav">
            <div class="sidenav-content">
                <a href="#card_1" class="nav-link">Card 1</a>
                <a href="#card_2" class="nav-link">Card 2</a>
                <a href="#card_3" class="nav-link">Card 3</a>
                <a href="#card_4" class="nav-link">Card 4</a>
                <a href="#card_5" class="nav-link">Card 5</a>
                <a href="#card_6" class="nav-link">Card 6</a>
                <a href="#card_7" class="nav-link">Card 7</a>
                <a href="#card_8" class="nav-link">Card 8</a>
                <a href="#card_9" class="nav-link">Card 9</a>
            </div>
        </div> -->
        <div class="row">

            <div id="card_1" class="col-lg-12 layout-spacing layout-top-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Tipo Movimiento</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <input type="hidden" name="idclientes" id="idclientes" value="<?= $_GET['cliente']; ?>">
                                    <td class="text-primary text-uppercase font-weight-bold">Movimiento</td>
                                    <td id="movimiento"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Persona</td>
                                    <td id="persona"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Numero celular</td>
                                    <td id="numcelular"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="card_2" class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Tipo evaluacion</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Plazo</td>
                                    <td id="plazo"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Forma de pago</td>
                                    <td id="formadepago"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="card_3" class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Datos personales</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Nombre o Razón Social</td>
                                    <td id="nombre"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Apellido Paterno</td>
                                    <td id="apaterno"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Apellido Materno</td>
                                    <td id="amaterno"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Fecha de nacimiento</td>
                                    <td id="fechanac"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">RFC</td>
                                    <td id="rfc"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Apoderado Legal</td>
                                    <td id="apoderado_legal"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">RFC Empresa</td>
                                    <td id="rfc_empresa"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="card_4" class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Domicilio personal</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Calle</td>
                                    <td id="calle"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Numero exterior</td>
                                    <td id="num_ext"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Numero interior</td>
                                    <td id="num_int"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Colonia</td>
                                    <td id="colonia"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Ciudad</td>
                                    <td id="ciudad"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Municipio</td>
                                    <td id="municipio"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Estado</td>
                                    <td id="estado"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold">Codigo Postal</td>
                                    <td id="codigo_postal"></td>
                                </tr>
                                <tr>
                                    <td class="text-primary text-uppercase font-weight-bold" >Numero de contacto del cliente</td>
                                    <td id="numcontacto"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="card_5" class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Informacion crediticia</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>¿Acepta cliente revisarlo en Buro de Credito?</td>
                                    <td id="buro_de_credito"></td>
                                </tr>
                                <tr>
                                    <td>¿Cuenta con Tarjeta de Credito?</td>
                                    <td id="tarjeta_credito"></td>
                                </tr>
                                <tr>
                                    <td>>¿Ha ejercido un credito Hipotecario?</td>
                                    <td id="credito_hipotecario"></td>
                                </tr>
                                <tr>
                                    <td>¿Ha ejercido un credito automotriz en los ultimos en los ultimos 24 meses?</td>
                                    <td id="credito_automotriz"></td>
                                </tr>
                                <tr>
                                    <td>>a Radiomovil Dipsa S.A. de C.V. (telcel) para que realice la verificacion de datos con la informacion proporcionada (vista domiciliaria y/o confirmacion telefonica), a traves del personal de telcel y/o de un tercero de su eleccion</td>
                                    <td id="autorizo1"></td>
                                </tr>
                                <tr>
                                    <td>a Radiomovil Dipsa S.A. de C.V. (telcel) y a Fianzas Guardiana Inbursa S.A. Grupo Financiero Inbursa para que individual e indistintamente lleve(n) a cabo de investigaciones y monitoreos periodicos sobre el comportamiento crediticio de mi como persona fisico y/o de la empresa que represento como persona moral. Asi mismo, DECLARO que conozco la naturaleza y alcance legal de la informacion que se solicitara y ACEPTO que este documento quede bajo propiedad de Telcel para efecto del cumplimiento con lo dispuesto en la Ley para Regular a las Sociedades de informacion crediticia y demas leyes aplicables. CONSIENTO que esta autorizacion se encuentre vigente por un periodo de 3 años contados a partir de su fecha de expedicion y, en todo caso, durante el tiempo que mantega una relacion juridica con TELCEL</td>
                                    <td id="autorizo2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="card_6" class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4 class="text-center">Complemento de datos de cliente para crear folio sisact</h4>
                                <h4>Datos personales</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>Lugar de nacimiento</td>
                                    <td name="lugar_nac"></td>
                                </tr>
                                <tr>
                                    <td>Tipo identificacion</td>
                                    <td id="tipo_identificacion"></td>
                                </tr>
                                <tr>
                                    <td>Folio identificacion</td>
                                    <td id="folio_identificacion"></td>
                                </tr>
                                <tr>
                                    <td>Sexo</td>
                                    <td id="sexo"></td>
                                </tr>
                                <tr>
                                    <td>CURP</td>
                                    <td id="curp"></td>
                                </tr>
                                <tr>
                                    <td>Correo</td>
                                    <td id="correo"></td>
                                </tr>
                                <tr>
                                    <td>Banco</td>
                                    <td id="banco"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="card_7" class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Domicilio personal</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">

                    </div>
                </div>
            </div>

            <div id="card_8" class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Empleo</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">

                    </div>
                </div>
            </div>

            <div id="card_9" class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Referencias</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">

                    </div>
                </div>
            </div>

            <div id="card_10" class="col-lg-12 col-12  layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="text-right">
                        <div class="widget-content widget-content-area">
                            <a href="javascript:void(0);" class="btn btn-outline-primary my-2 mr-2">MODIFICAR</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php require 'footer.php'; ?>
<script src="scripts/secliente.js"></script>