var tabla;

function init() {
    mostrarform(false);
    listar();
 
    $("#form").on("submit",function(e) {
        agregaryeditar(e);
    });
}

function limpiar() {
    $("#idplazo").val("");
    $("#descripcion").val("");
}

function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#rowplazo").hide();
        $("#rowform").show();
        $("#btnGuardar").prop("disabled",false);
    } else {
        $("#rowplazo").show();
        $("#rowform").hide();
    }
}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar() {
    tabla=$("#tblplazo").DataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Brt',
        buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdf'
                ],
        "ajax":
                {
                    url: '../ajax/plazo.php?op=listar',
                    type : "get",
                    dataType : "json",
                    error: function(e){
                        console.log(e.responseText);
                    }
                },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 15,
        "order": [[ 0, "desc" ]]
    });
}

function agregaryeditar(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled",true);
    var formData = new FormData($("#form")[0]);
 
    $.ajax({
        url: "../ajax/plazo.php?op=agregaryeditar",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
 
        success: function(datos)
        {                    
            console.log(datos);
            mostrarform(false);
            tabla.ajax.reload();
        }
 
    });
    limpiar();
}

function mostrar(idplazo)
{
    $.post("../ajax/plazo.php?op=mostrar",{idplazo : idplazo}, function(data, status)
    {
        data = JSON.parse(data);
        mostrarform(true);

        $("#idplazo").val(data.idmovimiento);
        $("#descripcion").val(data.descripcion);
    });
}

init();