var tabla;

function init() {
    mostrarform(false);
	listar();

	$("#form").on("submit",function(e) {
		agregaryeditar(e);
	});
}

function limpiar() {
    $("#idusuarios").val("");
    $("#nombre").val("");
    $("#apellidos").val("");
    $("#correo").val("");
    $("#password").val("");
    $("#role").val("");
}

function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#rowusuarios").hide();
        $("#rowform").show();
        $("#btnGuardar").prop("disabled",false);
    } else {
        $("#rowusuarios").show();
        $("#rowform").hide();
    }
}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar() {
    tabla=$("#tblusuarios").DataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Brt',
        buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdf'
                ],
        "ajax":
                {
                    url: '../ajax/usuarios.php?op=listar',
                    type : "get",
                    dataType : "json",
                    error: function(e){
                        console.log(e.responseText);
                    }
                },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 15,
        "order": [[ 0, "desc" ]]
    });
}

function agregaryeditar(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled",true);
    var formData = new FormData($("#form")[0]);

    $.ajax({
        url: "../ajax/usuarios.php?op=agregaryeditar",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
 
        success: function(datos)
        {                    
            console.log(datos);
            mostrarform(false);
            tabla.ajax.reload();
        }
 
    });
    limpiar();
}

function mostrar(idusuarios)
{
    $.post("../ajax/usuarios.php?op=mostrar",{idusuarios : idusuarios}, function(data, status)
    {
        data = JSON.parse(data);
        mostrarform(true);

        $("#idusuarios").val(data.idusuarios);
        $("#nombre").val(data.nombre);
        $("#apellidos").val(data.apellidos);
        $("#correo").val(data.correo);
        $("#role").val(data.role);
    });
}

init();