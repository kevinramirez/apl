var tabla;

function init() {
    mostrar( $("#idclientes").val() );
    listar();
    $("#fechanac").inputmask("9999-99-99");
    $("#form").on("submit",function(e) { //colocar form en la vista
        agregaryeditar(e);
    });
}

function limpiar() {
    $("#idclientes").val("");
    $("#movimiento").val("");
}

function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#rowmovimiento").hide();
        $("#rowform").show();
        $("#btnGuardar").prop("disabled",false);
    } else {
        $("#rowmovimiento").show();
        $("#rowform").hide();
    }
}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar() {
    tabla=$("#tblsecliente").DataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Brt',
        buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdf'
                ],
        "ajax":
                {
                    url: '../ajax/clientes.php?op=listar',
                    type : "get",
                    dataType : "json",
                    error: function(e){
                        console.log(e.responseText);
                    }
                },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 15,
        "order": [[ 0, "asc" ]]
    });
}

function agregaryeditar(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled",true);
    var formData = new FormData($("#form")[0]);
 
    $.ajax({
        url: "../ajax/clientes.php?op=agregaryeditar",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
 
        success: function(datos)
        {                    
            console.log(datos);
            mostrarform(false);
            tabla.ajax.reload();
        }
 
    });
    limpiar();
}

function mostrar(idclientes)
{
    $.post("../ajax/clientes.php?op=mostrar",{idclientes : idclientes}, function(data, status)
    {
        data = JSON.parse(data);

        $("#movimiento").text(data.movimiento);
        $("#persona").text(data.persona);
        $("#numcelular").text(data.celular);

        $("#plazo").text(data.plazo);
        $("#formadepago").text(data.formadepago);

        $("#nombre").text(data.nombre);
        $("#apaterno").text(data.apellidop);
        $("#amaterno").text(data.apellidom);
        $("#fechanac").text(data.fechanac);
        $("#rfc").text(data.rfc);

        $("#calle").text(data.calle);
    });
}

init();